import com.endava.internship.mocking.model.Payment;
import com.endava.internship.mocking.repository.InMemPaymentRepository;
import com.endava.internship.mocking.repository.InMemUserRepository;
import com.endava.internship.mocking.service.BasicValidationService;
import com.endava.internship.mocking.service.PaymentService;
import static org.junit.jupiter.api.Assertions.assertTrue;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.times;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.UUID;

import static org.mockito.ArgumentMatchers.any;


@ExtendWith(MockitoExtension.class)
@SuppressWarnings("all")
class PaymentServiceTest {

    private PaymentService service;

    @Spy
    private InMemUserRepository userRepository;

    @Spy
    private InMemPaymentRepository paymentRepository;

    @Spy
    private BasicValidationService validationService;

    @BeforeEach
    void setUp() {
        service = new PaymentService(userRepository, paymentRepository, validationService);
    }

    @Test
    void createPayment() {
        final Integer userId = 1;
        final Double amount = 20.0;
        assertTrue(service.createPayment(userId, amount) instanceof Payment);
        verify(validationService, times(1)).validateUserId(userId);
        verify(validationService, times(1)).validateAmount(amount);
        verify(userRepository, times(1)).findById(userId);
        verify(paymentRepository, times(1)).save(any());
    }

    @Test
    void editMessage() {
        final String message = "Test message";
        final Payment payment = new Payment(1, 10.0, message);
        final UUID uuid = payment.getPaymentId();
        paymentRepository.save(payment);
        assertTrue(service.editPaymentMessage(uuid, message) instanceof Payment);
        verify(validationService, times(1)).validateMessage(message);
        verify(validationService, times(1)).validatePaymentId(uuid);
        verify(paymentRepository, times(1)).editMessage(uuid, message);
    }

    @Test
    void getAllByAmountExceeding() {
        assertTrue(service.getAllByAmountExceeding(10.0) instanceof List);
        verify(paymentRepository, times(1)).findAll();
    }
}
