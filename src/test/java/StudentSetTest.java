import com.endava.internship.collectionImpl.CustomHashMap;
import com.endava.internship.collectionImpl.Student;
import com.endava.internship.collectionImpl.StudentSet;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Iterator;

import static org.junit.Assert.*;

public class StudentSetTest {

    private StudentSet students;
    private Student student1;

    @Before
    public void setUp() {
        students = new StudentSet();
        student1 = new Student("Vladimir", LocalDate.of(1998, 10, 12));
    }

    @Test
    public void isEmptySize_isEmptyWhenCreated_True() {
        assertEquals(0, students.size());
        assertTrue(students.isEmpty());
    }

    @Test
    public void contains_newObjectContainsNothing_False() {
        assertFalse(students.contains(student1));
    }

    @Test
    public void add_correctAddingObjects_True() {
        assertTrue(students.add(student1));
    }

    @Test
    public void contains_whenObjectIsIn_True() {
        students.add(student1);
        assertTrue(students.contains(student1));
    }

    @Test
    public void remove_removingExcitingObject_True() {
        students.add(student1);
        assertTrue(students.remove(student1));
    }

    @Test
    public void contains_searchingForNull_True() {
        students.add(student1);
        students.add(null);
        assertTrue(students.contains(null));
    }

    @Test
    public void contains_searchingForRemovedNull_False() {
        students.add(student1);
        students.add(null);
        students.remove(null);
        assertFalse(students.contains(null));
    }

    @Test
    public void resize_searchingForObjectsAfterTransporting_True() {
        final CustomHashMap map = new CustomHashMap();
        map.put(student1, new Object());
        map.put(null, new Object());
        map.resize();
        assertTrue(map.containsKey(student1));
        assertTrue(map.containsKey(null));
    }

    @Test
    public void contains_searchingForObjectAfterRemoving_False() {
        students.add(student1);
        students.remove(student1);
        assertFalse(students.contains(student1));
    }

    @Test
    public void add_tryingDuplicate_False() {
        students.add(student1);
        assertFalse(students.add(student1));
    }

    @Test
    public void contains_nullDuplications_False() {
        students.add(null);
        students.add(null);
        students.remove(null);
        assertFalse(students.contains(null));
    }

    @Test
    public void isEmpty_collectionMustBecomeEmpty_True() {
        students.add(student1);
        students.remove(student1);
        assertTrue(students.isEmpty());
        assertEquals(0, students.size());
    }

    @Test
    public void iterator_correctIterationCycle_True() {
        int count = 0;
        students.add(student1);
        students.add(null);
        final Iterator<Student> iterator = students.iterator();
        while(iterator.hasNext()) {
            iterator.next();
            count++;
        }
        assertEquals(2, count);
    }

    @Test
    public void clear_removingWholeObjects_True() {
        students.add(student1);
        students.clear();
        assertTrue(students.isEmpty());
    }

    @Test
    public void toArray_arrayMustContainSameObjects_True() {
        students.add(student1);
        students.add(null);
        final Student[] studentsArray = (Student[]) students.toArray();
        assertEquals(2, studentsArray.length);
        assertEquals(student1, studentsArray[0]);
        assertNull(studentsArray[1]);
    }

    @Test
    public void addAll_transportingEntriesFromCollection_True() {
        final HashSet<Student> set = new HashSet<>();
        set.add(student1);
        set.add(new Student("Ivan", LocalDate.of(1995, 12, 18)));
        set.add(new Student("Dmitrii", LocalDate.of(1984, 5, 5)));
        students.addAll(set);
        assertEquals(3, students.size());
    }

    @Test
    public void removeAll_deletingEntriesFromOurCollection_True() {
        final HashSet<Student> set = new HashSet<>();
        set.add(student1);
        set.add(new Student("Ivan", LocalDate.of(1995, 12, 18)));
        set.add(new Student("Dmitrii", LocalDate.of(1984, 5, 5)));
        students = new StudentSet(set);
        students.removeAll(set);
        assertTrue(students.isEmpty());
    }

    @Test
    public void containsAll_comparisonTwoCollections_True() {
        final HashSet<Student> set = new HashSet<>();
        set.add(student1);
        set.add(new Student("Ivan", LocalDate.of(1995, 12, 18)));
        set.add(new Student("Dmitrii", LocalDate.of(1984, 5, 5)));
        students = new StudentSet(set);
        assertTrue(students.containsAll(set));
    }
}
