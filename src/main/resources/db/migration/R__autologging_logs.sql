CREATE OR REPLACE TRIGGER update_logs
    AFTER INSERT OR DELETE
    ON EMPLOYEES
    for each row
BEGIN
    IF INSERTING
    THEN
        INSERT INTO employment_logs  (first_name, last_name, employment_action) VALUES ( :new.FIRST_NAME, :new.LAST_NAME, 'HIRED');
    ELSIF DELETING
    THEN
        INSERT INTO employment_logs  (first_name, last_name, employment_action) VALUES ( :new.FIRST_NAME, :new.LAST_NAME, 'FIRED');
    END IF;
END;