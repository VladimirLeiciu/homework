CREATE OR REPLACE TRIGGER update_amount_of_departments
    AFTER INSERT OR DELETE
    ON departments
BEGIN
    IF INSERTING
    THEN
        UPDATE locations SET department_amount  = (SELECT COUNT(*) FROM departments where LOCATIONS.LOCATION_ID = DEPARTMENTS.LOCATION_ID);
    ELSIF DELETING
    THEN
        UPDATE locations SET department_amount  = (SELECT COUNT(*) FROM departments where LOCATIONS.LOCATION_ID = DEPARTMENTS.LOCATION_ID);
    END IF;
END;