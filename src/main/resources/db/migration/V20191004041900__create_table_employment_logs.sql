CREATE SEQUENCE logs_seq START WITH 1 increment by 1;


create table employment_logs
(employment_log_id number(3) default logs_seq.nextval not null,
constraint id_pk primary key (employment_log_id),
first_name varchar2(20),
last_name varchar2(25) not null,
employment_action varchar2(10),
employment_status timestamp default current_timestamp,
constraint action_interval check ( employment_action in ('HIRED', 'FIRED') ));

