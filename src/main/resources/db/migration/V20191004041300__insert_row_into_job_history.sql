INSERT INTO regions VALUES   
        ( 1  
        , 'Europe'   
        );  

INSERT INTO countries VALUES   
        ( 'IT'  
        , 'Italy'  
        , 1   
        );  
        
INSERT INTO locations VALUES   
        ( 1000   
        , '1297 Via Cola di Rie'  
        , '00989'  
        , 'Roma'  
        , NULL  
        , 'IT'
        ); 

INSERT INTO departments VALUES   
        ( 90  
        , 'Administration'  
        , null  
        , 1000  
        );  

INSERT INTO departments VALUES   
        ( 20  
        , 'Marketing'  
        , null  
        , 1000  
        ); 
        

INSERT INTO jobs VALUES   
        ( 'AD_PRES'  
        , 'President'  
        , 20080  
        , 40000  
        );
        
INSERT INTO jobs VALUES   
        ( 'IT_PROG'  
        , 'Programmer'  
        , 20080  
        , 40000  
        ); 

INSERT INTO employees VALUES   
        ( 100  
        , 'Steven'  
        , 'King'  
        , 'SKING'  
        , '515.123.4567'  
        , TO_DATE('17-06-2003', 'dd-MM-yyyy')  
        , 'AD_PRES'  
        , 24000  
        , NULL  
        , NULL  
        , 90  
        );
        
INSERT INTO job_history  
VALUES (100  
       , TO_DATE('13-01-2001', 'dd-MM-yyyy')  
       , TO_DATE('24-07-2002', 'dd-MM-yyyy')  
       , 'IT_PROG'  
       , 20);
