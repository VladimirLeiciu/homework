
create table projects 
( project_id number (3) not null,
constraint project_id_pk primary key (project_id),
project_description varchar2(50), 
constraint moreThanTen check (length(project_description) > 10 ),
project_investments number(8, -3),
constraint moreThanNull check (project_investments > 0),
project_revenue number (8));


create table project_employee
(
project_id number (3),
constraint project_id_fk foreign key (project_id) references projects,
employee_id number (6, 0),
constraint employee_id_fk foreign key (employee_id) references employees,
hours_amount number (4)
);

