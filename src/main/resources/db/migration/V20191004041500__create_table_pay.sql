create table pay
(card_nr number(10) not null,
constraint card_nr_pk primary key (card_nr),
salary number(8, 2),
constraint salary_fk foreign key (salary) references employees,
commission_pct number(2, 2), 
constraint commission_pct_fk foreign key (commission_pct) references employees);
