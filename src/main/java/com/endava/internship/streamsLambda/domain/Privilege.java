package com.endava.internship.streamsLambda.domain;

public enum Privilege {
    CREATE, UPDATE, READ, DELETE
}