package com.endava.internship.collectionImpl;

import java.util.AbstractSet;
import java.util.Collection;
import java.util.Map;
import java.util.Set;

public class CustomHashMap implements Map<Student, Object> {

    private final Object CONST_VALUE = new Object();
    private Node<Student, Object>[] table;
    private int size;
    private final int defaultCapacity = 16;
    private int currentCapacity = defaultCapacity;
    private boolean nullIsDeleted;


    public CustomHashMap(int initialCapacity) {
        currentCapacity = initialCapacity;
        table = new Node[currentCapacity];
    }

    public CustomHashMap() {
        table = new Node[currentCapacity];
    }

    private static int hash(Object key){
        int h;
        return (key == null) ? 0 : (h = key.hashCode()) ^ (h >>> 16);
    }

    private int calculateBucket(int hash) {
        return (currentCapacity - 1) & hash;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public Object get(Object key) {
        int index = calculateBucket(hash(key));
        Node<Student, Object> node = table[index];
        while (node != null) {
            if (key == node.getKey() || (key.equals(node.getKey()) && hash(node.getKey()) == hash(key))) {
                return node.key;
            }
            node = node.next;
        }
        return node;
    }


    @Override
    public boolean containsKey(Object key) {
        int index = calculateBucket(hash(key));
        Node<Student, Object> node = table[index];

        while (node != null) {
            if (key == node.getKey() || key.equals(node.getKey()) && hash(node.getKey()) == hash(key)) {
                return key != null || !nullIsDeleted;
            }
            node = node.next;
        }
        return false;
    }

    @Override
    public boolean containsValue(Object value) {
        return false;
    }


    @Override
    public Object put(Student key, Object value) {
        if (size == table.length - 4) {
            resize();
        }

        if (key == null) {
            nullIsDeleted = false;
        }

        int bucketNum = calculateBucket(hash(key));
        Node<Student, Object> node = table[bucketNum];
        Node<Student, Object> prevNode = node;

        if (node == null) {
            table[bucketNum] = new Node<>(key, value, null);
            size++;
            return table[bucketNum].getKey();
        }

        while (node != null) {
            if(key == node.getKey() || (key.equals(node.getKey()) && hash(node.getKey()) == hash(key))) {
                node.key = key;
                return null;
            }
            prevNode = node;
            node = node.next;
        }

        prevNode.next = new Node<>(key, CONST_VALUE, null);
        size++;
        return prevNode.next.getKey();
    }


    @Override
    public void putAll(Map<? extends Student, ?> m) {
       for (Entry<? extends Student , ?> x : m.entrySet()) {
           put(x.getKey(), CONST_VALUE);
       }
    }

    @Override
    public Object remove(Object key) {
        int index = calculateBucket(hash(key));
        Node<Student, Object> node = table[index]; // checkout on existing, if is it decrease of size

        while (node != null) {
            if(key == node.getKey() || (key.equals(node.getKey()) && hash(node.getKey()) == hash(key))) {
                Student student = node.getKey();
                if (student == null) {
                    if (!nullIsDeleted) {
                        nullIsDeleted = true;
                        --size;
                    }
                    return null;
                }
                node.key = null;
                --size;
                return student;
            }
            node = node.next;
        }
        return node;
    }

    @Override
    public void clear() {
        table = new Node[defaultCapacity];
    }

    public void resize() {
        currentCapacity += 20; // increase length of table
        size = 0;
        Node[] oldTable = table;
        Node[] newTable = new Node[currentCapacity];
        table = newTable;

        for (Node<Student, Object> node : oldTable) {
            if (node != null) {
                put(node.getKey(), CONST_VALUE);
            }
        }
    }

    private static class Node<K, V> implements Entry<K, V>{
        private K key;
        private final V value;
        private Node<K, V> next;

        public Node(K key, V value, Node<K, V> node) {
            this.key = key;
            this.value = value;
            this.next = node;
        }

        @Override
        public K getKey() {
            return key;
        }

        @Override
        public V getValue() {
            return value;
        }

        @Override
        public V setValue(V value) {
            return null;
        }
    }

    public KeySet keySet() {
        return new KeySet();
    }

    @Override
    public Collection<Object> values() {
        return null;
    }

    @Override
    public Set<Entry<Student, Object>> entrySet() {
        return null;
    }

    public class KeySet extends AbstractSet {


        private Student[] students;

        KeySet() {
            students = collect();
        }

        private Student[] collect() {
            int count = 0;
            Student[] students = new Student[size];
            for (Node<Student, Object> x : table) {
                Node<Student, Object> node = x;
                while (node != null) {
                    if (node.getKey() != null) {
                        students[count] = node.getKey();
                        count++;
                    }
                    node = node.next;
                }
            }
            return students;
        }

        @Override
        public Iterator<Student> iterator() {
            Iterator<Student> iterator = new Iterator<>();
            iterator.setStudents(students);
            return iterator;
        }

        @Override
        public int size() {
            return size;
        }

        public Student[] getStudents() {
            return students;
        }

        private class Iterator<E> implements java.util.Iterator<E>{

            private E [] students;
            private int count;

            @Override
            public boolean hasNext() {
                return count <= students.length - 1;
            }

            @Override
            public E next() {
                E student = null;
                if (hasNext()) {
                    student = students[count];
                    count++;
                }
                return student;
            }

            private void setStudents(E[] studentsArray) {
                students = studentsArray;
            }
        }
    }
}
