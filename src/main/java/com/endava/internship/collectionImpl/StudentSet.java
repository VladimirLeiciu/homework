package com.endava.internship.collectionImpl;


import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

public class StudentSet implements Set<Student> {

    private CustomHashMap map;

    public StudentSet() {
        map = new CustomHashMap();
    }

    public StudentSet(HashMap<Student, Object> externalMap) {
        map = new CustomHashMap();
        map.putAll(externalMap);
    }

    public StudentSet(Collection<Student> collection) {
        map = new CustomHashMap();
        addAll(collection);
    }

    private Object get(Student key) {
        return map.get(key);
    }

    @Override
    public int size() {
        return map.size();
    }

    @Override
    public boolean isEmpty() {
        return map.isEmpty();
    }

    @Override
    public boolean contains(Object o) {
        return map.containsKey(o);
    }

    @Override
    public Iterator<Student> iterator() {
        return map.keySet().iterator();
    }

    @Override
    public Object[] toArray() {
        return map.keySet().getStudents();
    }

    @Override
    public <T> T[] toArray(T[] ts) {
        //Ignore this for homework
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean add(Student student) {
        return map.put(student, new Object()) == student;
    }

    @Override
    public boolean remove(Object o) {
        return map.remove(o) == o;
    }

    @Override
    public boolean containsAll(Collection<?> collection) {
        boolean isSame = true;
        for (Object obj : collection) {
            if (!contains(obj)) {
                isSame = false;
                break;
            }
        }
        return isSame;
    }

    @Override
    public boolean addAll(Collection<? extends Student> collection) {
        boolean modified = false;
        for (Student student : collection) {
            if (add(student)) {
                modified = true;
            }
        }
        return modified;
    }

    @Override
    public boolean retainAll(Collection<?> collection) {
        //Ignore this for homework
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean removeAll(Collection<?> collection) {
        boolean collectionChanged = false;
        Object[] objects = collection.toArray();
        for (Object obj : objects) {
            if (remove(obj)) {
                collectionChanged = true;
            }
        }
        return collectionChanged;
    }

    @Override
    public void clear() {
        map = new CustomHashMap();
    }

    @Override
    public String toString() {
        return "StudentSet{" +
                "map=" + map +
                '}';
    }

    @Override
    public int hashCode() {
        int h = 0;
        Iterator<Student> iterator = iterator();
        while (iterator.hasNext()) {
            Student student = iterator.next();
            if (student != null)
                h += student.hashCode();
        }
        return h;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;

        if (!(o instanceof Set))
            return false;
        Collection<?> c = (Collection<?>) o;
        if (c.size() != size())
            return false;
        try {
            return containsAll(c);
        } catch (ClassCastException | NullPointerException unused)   {
            return false;
        }
    }
}
