-- Write a query to display: 
-- 1. the first name, last name, salary, and job grade for all employees.
SELECT first_name, last_name, salary, job_title
FROM employees
         LEFT JOIN jobs USING (job_id);
-- 2. the first and last name, department, city, and state province for each employee.
SELECT e.first_name, e.last_name, e.department_id, d.department_name, d.location_id, l.city, l.state_province
FROM employees e, departments d, locations l
where e.department_id = d.department_id and d.location_id = l.location_id;
-- 3. the first name, last name, department number and department name, for all employees for departments 80 or 40.
SELECT e.first_name, e.last_name, e.department_id, d.department_name
FROM employees e, departments d
where e.department_id = d.department_id and (e.department_id = 80 or e.department_id = 40);
-- 4. those employees who contain a letter z to their first name and also display their last name, department, city, and state province.
select e.first_name, e.last_name, d.department_name, l.city, l.state_province
FROM employees e, departments d, locations l
where e.department_id = d.department_id and d.location_id = l.location_id and e.first_name like '%z%';
-- 5. the first and last name and salary for those employees who earn less than the employee earn whose number is 182.
select e.first_name, e.last_name, e.salary
from employees e
join employees s
on e.salary < s.salary
and s.employee_id = 182;
-- 6. the first name of all employees including the first name of their manager.
select e.first_name, m.first_name
from employees e
join employees m
on e.manager_id = m.employee_id;
-- 7. the first name of all employees and the first name of their manager including those who does not working under any manager.
select e.first_name, m.first_name
from employees e
left outer join employees m
on e.manager_id = m.employee_id;
-- 8. the details of employees who manage a department.
select e.first_name, e.last_name
from departments d, employees e
where d.manager_id = e.employee_id;
-- 9. the first name, last name, and department number for those employees who works in the same department as the employee who holds the last name as Taylor.
select e.first_name, e.last_name, e.department_id
from employees e
join employees t
on e.department_id = t.department_id
and t.last_name = 'Taylor';
--10. the department name and number of employees in each of the department.
select d.department_name, count(*) as "Number"
from departments d
inner join employees e
on d.department_id = e.department_id
group by d.department_id, d.department_name
order by "Number";
--11. the name of the department, average salary and number of employees working in that department who got commission.
SELECT department_name, AVG(salary), COUNT(commission_pct) 
	FROM departments 
		JOIN employees USING (department_id) 
GROUP BY department_name;
--12. job title and average salary of employees.
SELECT job_title, AVG(salary) 
FROM employees 
NATURAL JOIN jobs 
GROUP BY job_title;
--13. the country name, city, and number of those departments where at least 2 employees are working.
SELECT country_name,city, COUNT(department_id)
	FROM countries 
		JOIN locations USING (country_id) 
		JOIN departments USING (location_id) 
WHERE department_id IN 
    (SELECT department_id 
		FROM employees 
	 GROUP BY department_id 
	 HAVING COUNT(department_id)>=2)
GROUP BY country_name,city;
--14. the employee ID, job name, number of days worked in for all those jobs in department 80.
SELECT employee_id, job_title, end_date-start_date DAYS 
	FROM job_history 
		NATURAL JOIN jobs 
			WHERE department_id=80;
--15. the name ( first name and last name ) for those employees who gets more salary than the employee whose ID is 163.
select e.first_name, e.last_name
from employees e
join employees s
on e.salary > s.salary
and s.employee_id = 163;
--16. the employee id, employee name (first name and last name ) for all employees who earn more than the average salary.
select e.first_name, e.last_name
from employees e
where e.salary > (select avg(salary) from employees);
--17. the employee name ( first name and last name ), employee id and salary of all employees who report to Payam.
SELECT first_name, last_name, employee_id, salary  
FROM employees  
WHERE manager_id = 
(SELECT employee_id  
FROM employees  
WHERE first_name = 'Payam');
--18. the department number, name ( first name and last name ), job and department name for all employees in the Finance department.
select e.department_id, e.first_name, e.last_name, d.department_name
from employees e, departments d
where e.department_id = d.department_id
and d.department_name = 'Finance';
--19. all the information of an employee whose id is any of the number 134, 159 and 183.
select e.first_name, e.last_name, e.salary
from employees e
where e.employee_id in (134, 159, 183);
--20. all the information of the employees whose salary is within the range of smallest salary and 2500.
select e.first_name, e.last_name, e.salary
from employees e
where e.salary >
( select min (e.salary)
from employees e)
and e.salary < 2500;
--21. all the information of the employees who does not work in those departments where some employees works whose id within the range 100 and 200.
SELECT * 
FROM employees 
WHERE department_id NOT IN 
(SELECT department_id 
FROM departments 
WHERE manager_id BETWEEN 100 AND 200);
--22. all the information for those employees whose id is any id who earn the second highest salary.
SELECT * 
FROM employees 
WHERE employee_id IN 
(SELECT employee_id 
FROM employees  
WHERE salary = 
(SELECT MAX(salary) 
FROM employees 
WHERE salary < 
(SELECT MAX(salary) 
FROM employees)));
--23. the employee name( first name and last name ) and hiredate for all employees in the same department as Clara. Exclude Clara.
select e.first_name, e.last_name, e.department_id
from employees e
join employees c
on c.first_name = 'Clara'
and e.department_id = c.department_id and e.first_name != 'Clara';
--24. the employee number and name( first name and last name ) for all employees who work in a department with any employee whose name contains a T.
select e.employee_id, e.first_name, e.last_name
from departments d
join employees e
on d.department_id = e.department_id
and e.first_name not like '%t%';
--25. full name(first and last name), job title, starting and ending date of last jobs for those employees with worked without a commission percentage.
select e.first_name, e.last_name, j.job_title, h.start_date, h.end_date
from employees e
join jobs j
on e.job_id = j.job_id
and e.commission_pct is null
join job_history h
on j.job_id = h.job_id;
--26. the employee number, name( first name and last name ), and salary for all employees who earn more than the average salary and who work in a department with any employee with a J in their name.
SELECT employee_id, first_name , salary  
FROM employees  
WHERE salary > 
(SELECT AVG (salary)  
FROM employees ) 
AND  department_id IN 
( SELECT department_id  
FROM employees  
WHERE first_name LIKE '%J%');
--27. the employee number, name( first name and last name ) and job title for all employees whose salary is smaller than any salary of those employees whose job title is MK_MAN.
select e.employee_id, e.first_name, e.last_name, j.job_title
from employees e
join jobs j
on e.job_id = j.job_id
and e.salary < (
select j.min_salary
from employees e
join jobs j
on e.job_id = j.job_id
and j.job_id = 'MK_MAN');
--28. the employee number, name( first name and last name ) and job title for all employees whose salary is smaller than any salary of those employees whose job title is MK_MAN. Exclude Job title MK_MAN.
select e.employee_id, e.first_name, e.last_name, j.job_title
from employees e
join jobs j
on e.job_id = j.job_id
and j.job_id != 'MK_MAN'
and e.salary < (
select j.min_salary
from employees e
join jobs j
on e.job_id = j.job_id
and j.job_id = 'MK_MAN');
--29. all the information of those employees who did not have any job in the past.
select *
from employees e
where e.employee_id not in (select employee_id from job_history);
--30. the employee number, name( first name and last name ) and job title for all employees whose salary is more than any average salary of any department.
SELECT employee_id, first_name, last_name, job_id  
FROM employees  
WHERE salary > ALL  
( SELECT AVG(salary)  
FROM employees  
GROUP BY department_id 
);
--31. the employee id, name ( first name and last name ) and the job id column with a modified title SALESMAN for those employees whose job title is ST_MAN and DEVELOPER for whose job title is IT_PROG.
SELECT  employee_id,  first_name, last_name,  
CASE job_id  
WHEN 'ST_MAN' THEN 'SALESMAN'  
WHEN 'IT_PROG' THEN 'DEVELOPER'  
ELSE job_id  
END AS designation,  salary 
FROM employees;
--32. the employee id, name ( first name and last name ), salary and the SalaryStatus column with a title HIGH and LOW respectively for those employees whose salary is more than and less than the average salary of all employees.
SELECT  employee_id,  first_name, last_name,  salary AS SalaryDrawn,  
ROUND((salary -(SELECT AVG(salary) FROM employees)),2) AS AvgCompare,  
CASE  WHEN salary >= 
(SELECT AVG(salary) 
FROM employees) THEN 'HIGH'  
ELSE 'LOW'  
END AS SalaryStatus 
FROM employees;

--33. the employee id, name ( first name and last name ), SalaryDrawn, AvgCompare (salary - the average salary of all employees)
    -- and the SalaryStatus column with a title HIGH and LOW respectively for those employees whose salary is more than and less than
    -- the average salary of all employees.
SELECT  employee_id,  first_name, last_name,  salary AS SalaryDrawn,  
ROUND((salary -(SELECT AVG(salary) FROM employees)),2) AS AvgCompare,  
CASE  WHEN salary >= 
(SELECT AVG(salary) 
FROM employees) THEN 'HIGH'  
ELSE 'LOW'  
END AS SalaryStatus 
FROM employees;
--34. all the employees who earn more than the average and who work in any of the IT departments.
select *
from employees e
where e.salary > (select avg(s.salary) from employees s)
and e.department_id = 60;
--35. who earns more than Mr. Ozer.
select *
from employees e
join employees o
on o.last_name = 'Ozer'
and e.salary > o.salary;
--36. which employees have a manager who works for a department based in the US.
select d.manager_id
from departments d
join locations l
on d.location_id = l.location_id
and l.country_id = 'US'
and d.manager_id is not null;
--37. the names of all employees whose salary is greater than 50% of their department’s total salary bill.
SELECT e1.first_name, e1.last_name 
FROM employees e1 
WHERE salary > 
( SELECT (SUM(salary))*.5 
FROM employees e2 
WHERE e1.department_id=e2.department_id);
--38. the employee id, name ( first name and last name ), salary, department name and city for all
--the employees who gets the salary as the salary earn by the employee which is maximum within the joining person January 1st, 2002 and December 31st, 2003.
SELECT a.employee_id, a.first_name, a.last_name, a.salary, b.department_name, c.city  
FROM employees a, departments b, locations c  
WHERE a.salary =  
(SELECT MAX(salary) 
FROM employees 
WHERE hire_date BETWEEN '01/01/2002' AND '12/31/2003') 
AND a.department_id=b.department_id 
AND b.location_id=c.location_id;
--39. the first and last name, salary, and department ID for all those employees who earn more than the average salary and arrange the list in descending order on salary.
SELECT first_name, last_name , salary, department_id 
FROM employees 
WHERE salary > (
SELECT AVG(salary)
FROM employees )
ORDER BY salary DESC;
--40. the first and last name, salary, and department ID for those employees who earn more than the maximum salary of a department which ID is 40.
select e.first_name, e.last_name, e.salary, e.department_id
from employees e
where e.salary > (select max(salary) from employees where department_id = 40);
--41. the department name and Id for all departments where they located, that Id is equal to the Id for the location where department number 30 is located.
select d.department_id, d.department_name, d.location_id
from departments d
join departments f
on d.location_id = f.location_id
and f.department_id = 30;
--42. the first and last name, salary, and department ID for all those employees who work in that department where the employee works who hold the ID 201.
select *
from employees f
where f.department_id = (
select e.department_id
from employees e
where e.employee_id = 201);

select *
from employees e
join employees f
on e.department_id = f.department_id
and f.employee_id = 201;
--43. the first and last name, salary, and department ID for those employees whose salary is equal to the salary of the employee who works in that department which ID is 40.
select *
from employees e
join employees f
on e.salary = f.salary
and f.department_id = 40;
--44. the first and last name, salary, and department ID for those employees who earn more than the minimum salary of a department which ID is 40.
select * 
from employees
where salary > (
select min(salary)
from employees
where department_id = 40);
--45. the first and last name, salary, and department ID for those employees who earn less than the minimum salary of a department which ID is 70.
select * 
from employees
where salary < (
select min(salary)
from employees
where department_id = 70);
--46. the first and last name, salary, and department ID for those employees who earn less than the average salary, and also work at the department where the employee Laura is working as a first name holder.
select * 
from employees
where salary < (
select avg(e.salary)
from employees e
join employees f
on e.department_id = f.department_id
and f.first_name = 'Laura');
--47. the full name (first and last name) of manager who is supervising 4 or more employees.
SELECT first_name, last_name
FROM employees 
WHERE employee_id IN 
(SELECT manager_id 
FROM employees 
GROUP BY manager_id 
HAVING COUNT(*)>=4);
--48. the details of the current job for those employees who worked as a Sales Representative in the past.
SELECT * 
FROM jobs 
WHERE job_id IN 
(SELECT job_id 
FROM employees 
WHERE employee_id IN 
(SELECT employee_id 
FROM job_history 
WHERE job_id='SA_REP'));
--49. all the infromation about those employees who earn second lowest salary of all the employees.
SELECT *
FROM employees m
WHERE  2 = (SELECT COUNT(DISTINCT salary ) 
FROM employees
WHERE  salary <= m.salary);
--50. the department ID, full name (first and last name), salary for those employees who is highest salary drawar in a department.
SELECT department_id, first_name || ' ' || last_name AS Employee_name, salary 
FROM employees a
WHERE salary = 
(SELECT MAX(salary) 
FROM employees 
WHERE department_id = a.department_id);
